unit details;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLite3Conn, SQLDB, DB, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, DBGrids, StdCtrls, Buttons, DBCtrls, Menus, PairSplitter, ComCtrls,
  Grids, newcontact, maillog, send, about;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    dsrcSource: TDataSource;
    imgMugshot: TImage;
    imlUI: TImageList;
    lblSignalMessage: TLabel;
    lblSignalLevel: TLabel;
    lblSysName: TLabel;
    lblNotes: TLabel;
    lblType: TLabel;
    lblContactName: TLabel;
    icnSignal: TSpeedButton;
    lstContacts: TListBox;
    mnuMain: TMainMenu;
    MenuItem1: TMenuItem;
    mnuQuit: TMenuItem;
    mnuNewMail: TMenuItem;
    mnuNewContact: TMenuItem;
    mnuLogs: TMenuItem;
    MenuItem14: TMenuItem;
    mnuAbout: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    mnuSignalNone: TMenuItem;
    mnuSignalWeak: TMenuItem;
    mnuSignalMedium: TMenuItem;
    mnuSignalStrong: TMenuItem;
    PairSplitter1: TPairSplitter;
    pspUpperHalf: TPairSplitterSide;
    pspLowerHalf: TPairSplitterSide;
    pnlPicture: TPanel;
    connDB: TSQLite3Connection;
    qryDB: TSQLQuery;
    trnGet: TSQLTransaction;
    procedure btnAddContactClick(Sender: TObject);
    procedure btnChatLogClick(Sender: TObject);
    procedure btnNewChatClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lstContactsSelectionChange(Sender: TObject; User: boolean);
    procedure mnuAboutClick(Sender: TObject);
    procedure mnuLogsClick(Sender: TObject);
    procedure mnuNewContactClick(Sender: TObject);
    procedure mnuNewMailClick(Sender: TObject);
    procedure mnuQuitClick(Sender: TObject);
    procedure mnuSignalMediumClick(Sender: TObject);
    procedure mnuSignalNoneClick(Sender: TObject);
    procedure mnuSignalStrongClick(Sender: TObject);
    procedure mnuSignalWeakClick(Sender: TObject);
  private

  public

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.btnAddContactClick(Sender: TObject);
begin
  frmNewContact.ShowModal;
  FormCreate(Sender);
end;

procedure TfrmMain.btnChatLogClick(Sender: TObject);
begin
  frmMailLog.ShowModal;
end;

procedure TfrmMain.btnNewChatClick(Sender: TObject);
begin
  frmNewMail.ShowModal;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  curName: String;
begin
  qryDB.Close;
  trnGet.Active := False;
  connDB.Connected := False;
  qryDB.SQL.Text := 'select * from contacts';
  qryDB.Open;
  lstContacts.Items.Clear;
  while not qryDB.EOF do
  begin
    curName := qryDB.FieldByName('name').AsString;
    lstContacts.Items.Add(curName);
    qryDB.Next;
  end;
  qryDB.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  imgMugshot.Picture.LoadFromFile(GetCurrentDir + '/profiles/no-photo.png');
end;

procedure TfrmMain.lstContactsSelectionChange(Sender: TObject; User: boolean);
  var
  mugshotName: String;
begin
  qryDB.Close;
  trnGet.Active := False;
  connDB.Connected := False;
  qryDB.SQL.Text := 'select * from contacts where id = :CNTID';
  qryDB.ParamByName('CNTID').AsInteger := lstContacts.ItemIndex;
  trnGet.Active := True;
  connDB.Connected := True;
  qryDB.Open;
  if not qryDB.EOF then
  begin
    lblContactName.Caption := qryDB.FieldByName('name').AsString;
    lblType.Caption := qryDB.FieldByName('type').AsString;
    lblType.Visible := True;
    if (qryDB.FieldByName('hassystem').AsInteger = 0) then
      lblSysName.Visible := False
    else
    begin
      lblSysName.Visible := True;
      lblSysName.Caption := qryDB.FieldByName('systemname').AsString;
    end;
    if not qryDB.FieldByName('notes').IsNull then lblNotes.Caption := qryDB.FieldByName('notes').AsString
    else lblNotes.Caption := 'No notes';
    lblNotes.Visible := True;
    mugshotName := GetCurrentDir + '/profiles/' + lblContactName.Caption + '.png';
    if FileExists(mugshotName) then imgMugshot.Picture.LoadFromFile(mugshotName)
    else imgMugshot.Picture.LoadFromFile(GetCurrentDir + '/profiles/no-photo.png');
  end
  else ShowMessage('Nothing to pull!');
  qryDB.Close;
  trnGet.Active := False;
  connDB.Connected := False;
end;

procedure TfrmMain.mnuAboutClick(Sender: TObject);
begin
  frmAbout.ShowModal;
end;

procedure TfrmMain.mnuLogsClick(Sender: TObject);
begin
  frmMailLog.ShowModal;
end;

procedure TfrmMain.mnuNewMailClick(Sender: TObject);
begin
  frmNewMail.ShowModal;
end;

procedure TfrmMain.mnuNewContactClick(Sender: TObject);
begin
  frmNewContact.ShowModal;
end;

procedure TfrmMain.mnuQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.mnuSignalMediumClick(Sender: TObject);
begin
  icnSignal.ImageIndex := 3;
  lblSignalLevel.Caption := 'Signal level: Medium';
  lblSignalMessage.Caption := 'Troubleshooting recommended.';
end;

procedure TfrmMain.mnuSignalStrongClick(Sender: TObject);
begin
  icnSignal.ImageIndex := 4;
  lblSignalLevel.Caption := 'Signal level: Strong';
  lblSignalMessage.Caption := 'No troubleshooting needed. Go you!';
end;

procedure TfrmMain.mnuSignalNoneClick(Sender: TObject);
begin
  icnSignal.ImageIndex := 1;
  lblSignalLevel.Caption := 'Signal level: None';
  lblSignalMessage.Caption := 'Urgent troubleshooting needed.';
end;

procedure TfrmMain.mnuSignalWeakClick(Sender: TObject);
begin
  icnSignal.ImageIndex := 2;
  lblSignalLevel.Caption := 'Signal level: Weak';
  lblSignalMessage.Caption := 'Troubleshooting required.';
end;

end.
