# MSD - Mental Speed Dial

Simple logging tool for more... personal experiments. Coded in Free Pascal (Lazarus 2.2.6). Can be cross-compiled to Windows or Mac OS X.

Licensed under the MIT License.
