unit send;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, SQLite3Conn, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons;

type

  { TfrmNewMail }

  TfrmNewMail = class(TForm)
    cmbDestination: TComboBox;
    btnCancel: TSpeedButton;
    qryRegisterMsg: TSQLQuery;
    connDB: TSQLite3Connection;
    trnGet: TSQLTransaction;
    txtSensation: TEdit;
    lblMessage: TLabel;
    lblTo: TLabel;
    txtMessage: TMemo;
    btnSend: TSpeedButton;
    btnAttach: TSpeedButton;
    procedure btnAttachClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  frmNewMail: TfrmNewMail;

implementation

{$R *.lfm}

{ TfrmNewMail }

procedure TfrmNewMail.btnCancelClick(Sender: TObject);
begin
  frmNewMail.Close;
end;

procedure TfrmNewMail.btnAttachClick(Sender: TObject);
begin
  txtSensation.Enabled := not txtSensation.Enabled;
  if txtSensation.Enabled then
  begin
    btnAttach.Caption := 'Cancel';
    txtSensation.SetFocus;
  end
  else
  begin
    btnAttach.Caption := 'Attach feeling/sensation';
    txtSensation.Text := '';
    txtMessage.SetFocus;
  end;
end;

procedure TfrmNewMail.btnSendClick(Sender: TObject);
begin
  qryRegisterMsg.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  qryRegisterMsg.SQL.Text := 'insert into telemail(destid, contents, attachment) values (:DESTID, :CONTENTS, :ATTACH)';
  qryRegisterMsg.ParamByName('DESTID').AsInteger := cmbDestination.ItemIndex;
  qryRegisterMsg.ParamByName('CONTENTS').AsString := txtMessage.Text;
  if not (txtSensation.Text = '') then qryRegisterMsg.ParamByName('ATTACH').AsString := txtSensation.Text;
  connDB.Connected := True;
  trnGet.Active := True;
  qryRegisterMsg.ExecSQL;
  trnGet.Commit;
  qryRegisterMsg.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  ShowMessage('Message registered.');
  Close;
end;

procedure TfrmNewMail.FormCreate(Sender: TObject);
begin
  qryRegisterMsg.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  qryRegisterMsg.SQL.Text := 'select * from contacts';
  connDB.Connected := True;
  trnGet.Active := True;
  qryRegisterMsg.Open;
  while not qryRegisterMsg.EOF do
  begin
    cmbDestination.Items.Add(qryRegisterMsg.FieldByName('name').AsString);
    qryRegisterMsg.Next;
  end;
  qryRegisterMsg.Close;
  connDB.Connected := False;
  trnGet.Active := False;
end;

end.

