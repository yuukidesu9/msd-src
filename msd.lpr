program msd;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, details, send, newcontact, maillog, about
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmNewMail, frmNewMail);
  Application.CreateForm(TfrmNewContact, frmNewContact);
  Application.CreateForm(TfrmMailLog, frmMailLog);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.Run;
end.

