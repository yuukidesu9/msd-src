unit maillog;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, SQLite3Conn, Forms, Controls, Graphics, Dialogs,
  Buttons, ExtCtrls, StdCtrls;

type

  { TfrmMailLog }

  TfrmMailLog = class(TForm)
    cmbName: TComboBox;
    lblConvo: TLabel;
    nbConversation: TNotebook;
    btnPrev: TSpeedButton;
    btnNext: TSpeedButton;
    qryGetMessages: TSQLQuery;
    connDB: TSQLite3Connection;
    trnGet: TSQLTransaction;
    procedure btnNextClick(Sender: TObject);
    procedure btnPrevClick(Sender: TObject);
    procedure cmbNameChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  frmMailLog: TfrmMailLog;

implementation

{$R *.lfm}

{ TfrmMailLog }

procedure TfrmMailLog.FormCreate(Sender: TObject);
begin
  qryGetMessages.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  qryGetMessages.SQL.Text := 'select * from contacts';
  connDB.Connected := True;
  trnGet.Active := True;
  qryGetMessages.Open;
  cmbName.Clear;
  while not qryGetMessages.EOF do
  begin
    cmbName.Items.Add(qryGetMessages.FieldByName('name').AsString);
    qryGetMessages.Next;
  end;
  qryGetMessages.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  if (nbConversation.PageIndex < 0) then
    btnPrev.Enabled := False;
  if (nbConversation.PageIndex = nbConversation.PageCount-1) then
    btnNext.Enabled := False;
end;

procedure TfrmMailLog.btnNextClick(Sender: TObject);
begin
  if nbConversation.PageIndex < nbConversation.PageCount-1 then
    nbConversation.PageIndex := nbConversation.PageIndex + 1;
end;

procedure TfrmMailLog.btnPrevClick(Sender: TObject);
begin
  if nbConversation.PageIndex > 0 then
    nbConversation.PageIndex := nbConversation.PageIndex - 1;
end;

procedure TfrmMailLog.cmbNameChange(Sender: TObject);
var
  newPage: LongInt;
  lblMessage: TLabel;
  page: TMemo;
  message: String;

begin
  qryGetMessages.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  nbConversation.Pages.Clear;
  qryGetMessages.SQL.Text := 'select * from telemail where destid = :DESTID';
  qryGetMessages.Params.ParamByName('DESTID').AsInteger := cmbName.ItemIndex;
  connDB.Connected := True;
  trnGet.Active := True;
  qryGetMessages.Open;
  if qryGetMessages.EOF then
    ShowMessage('No messages to show, unfortunately.')
  else
  begin
    while not qryGetMessages.EOF do
    begin
      newPage := nbConversation.Pages.Add(qryGetMessages.FieldByName('msgid').ToString);      { msgid, destid, contents, attachment }

      page := TMemo.Create(nbConversation);
      page.Parent := nbConversation.Page[newPage];
      page.Align := alClient;
      page.ReadOnly := True;
      page.Enabled := False;

      page.Append('Message ID: ' + qryGetMessages.FieldByName('msgid').AsString);
      page.Append('Contents:' + LineEnding + LineEnding + qryGetMessages.FieldByName('contents').AsString);

      if not (qryGetMessages.FieldByName('attachment').AsString = '') then
        page.Append(LineEnding + 'Attached feeling/sensation: ' +  qryGetMessages.FieldByName('attachment').AsString);

      qryGetMessages.Next;
    end;
  end;
  qryGetMessages.Close;
  connDB.Connected := False;
  trnGet.Active := False;
  if (nbConversation.PageIndex < 0) then
    btnPrev.Enabled := False;
  if (nbConversation.PageIndex = nbConversation.PageCount-1) then
    btnNext.Enabled := False;
end;

end.

