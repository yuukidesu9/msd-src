unit newcontact;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, SQLite3Conn, Forms, Controls, Graphics, Dialogs,
  ExtDlgs, ExtCtrls, StdCtrls, Buttons;

type

  { TfrmNewContact }

  TfrmNewContact = class(TForm)
    chkSystem: TCheckBox;
    cmbContactType: TComboBox;
    dlgGetMugshot: TOpenPictureDialog;
    imgMugshot: TImage;
    lblContactType: TLabel;
    lblNotes: TLabel;
    btnAddContact: TSpeedButton;
    btnClear: TSpeedButton;
    pnlMugshot: TPanel;
    qryAdd: TSQLQuery;
    connDB: TSQLite3Connection;
    trnGet: TSQLTransaction;
    txtNotes: TMemo;
    txtSysName: TLabeledEdit;
    txtName: TLabeledEdit;
    procedure btnAddContactClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure chkSystemChange(Sender: TObject);
    procedure imgMugshotClick(Sender: TObject);
  private

  public

  end;

var
  frmNewContact: TfrmNewContact;
  mugshotfile: String;

implementation

{$R *.lfm}

{ TfrmNewContact }

procedure TfrmNewContact.imgMugshotClick(Sender: TObject);
begin
  if dlgGetMugshot.Execute then
  begin
    if fileExists(dlgGetMugshot.Filename) then
    begin
      mugshotfile := dlgGetMugshot.Filename;
      imgMugshot.Picture.LoadFromFile(mugshotfile);
      imgMugshot.Refresh;
    end;
  end;
end;

procedure TfrmNewContact.chkSystemChange(Sender: TObject);
begin
  if chkSystem.Checked then
    txtSysName.Visible := True
  else txtSysName.Visible := False;
end;

procedure TfrmNewContact.btnClearClick(Sender: TObject);
begin
  txtName.Text := '';
  txtSysName.Text := '';
  chkSystem.Checked := False;
  imgMugshot.Picture := Nil;
  imgMugshot.Refresh;
  txtNotes.Text := '';
  cmbContactType.ItemIndex := -1;
end;

procedure TfrmNewContact.btnAddContactClick(Sender: TObject);
begin
  qryAdd.Close;
  trnGet.Active := False;
  connDB.Connected := False;
  qryAdd.SQL.Text := 'insert into contacts (name, type, hassystem, systemname, notes) values (:CNTNAME, :CNTTYPE, :HASSYS, :SYSNAME, :NOTES)';
  qryAdd.Params.ParamByName('CNTNAME').AsString := txtName.Text;
  qryAdd.Params.ParamByName('CNTTYPE').AsString := cmbContactType.Text;
  qryAdd.Params.ParamByName('HASSYS').AsBoolean := chkSystem.Checked;
  if chkSystem.Checked then qryAdd.Params.ParamByName('SYSNAME').AsString := txtSysName.Text
  else qryAdd.Params.ParamByName('SYSNAME').AsString := 'no known system';
  qryAdd.Params.ParamByName('NOTES').AsString := txtNotes.Text;
  imgMugshot.Picture.SaveToFile(GetCurrentDir + '/' + txtName.Text + '.png');
  connDB.Connected := True;
  trnGet.Active := True;
  qryAdd.ExecSQL;
  trnGet.Commit;
  qryAdd.Close;
  trnGet.Active := False;
  connDB.Connected := False;
  ShowMessage('Contact added successfully.');
  btnClearClick(Sender);
  Close;
end;

end.

